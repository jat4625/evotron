import serial
import time
from torr_utils.singletonType import singleton
class CoreXYInterface(object, metaclass=singleton):
    """
    Function to create tool path between two points,
    only 'pick' argument to determine directionality of pick and place.
    (Whether it is picking from plate to well or vice versa)
    """

    def __init__(self):
        self._calibrated = False
        self._SterilePos = [0, 0]
        self._routines = []
        self._machineSt = _machineState()

    @classmethod
    def calibrate(cls):
        return '(Calibration procedure)'
    def setSterilePosition(self, pos=[]):
        if len(pos) < 2:
            self._SterilePos = pos
        else:
            cur = self._machineSt.getPosition()
            self._SterilePos = cur

    def sterilize(self):
        #define routine to perform sterilization between picks
        self._machineSt.write(self.move([self._machineSt.getPosition(), self._SterilePos]))
        return "(sterilize)\n"
    def pick(self):
        return "(pick)\n" #returns gcode string for pick operation
    def place(self):
        return "(place)\n" #returns gcode string for place operation
    def LinearMove(self, axis, level=1, spacing= 0):
        if axis in ("x","X",0):
            pass
        elif axis in ("y","Y",1):
            pass
        else:
            raise Exception("LinearMove:Not a valid Axis")
        x = _machineState().getPosition()
        gcode = self.move([[10,30], [20,30]])

        pass
    def pickMove(self, Arr):
        proto = ""
        for i in Arr:
            gc_move = lambda x, y: "G01 X{0:.3f} Y{1:.3f}\n".format(float(x), float(y))
            proto += gc_move(i[0][0], i[0][1])
            proto += self.pick()
            proto += gc_move(i[1][0], i[1][1])
            proto += self.place()
            proto += self.sterilize()
        return proto  # accepts a list of lists and returns a string of move instructions

    def move(self, Arr):
        proto = ""
        for i in Arr:
            gc_move = lambda x, y: "G01 X{0:.3f} Y{1:.3f}\n".format(float(x), float(y))
            proto += gc_move(i[0][0],i[0][1])

            proto += gc_move(i[1][0],i[1][1])

            proto += self.sterilize()
        return proto # accepts a list of lists and returns a string of move instructions

    def GcodeCompile(self, params, pick = False):
        #Generate and compile gcode for transport
        #NOTE: movement gcode is agnostic to where it is moving material
        if pick:
            compiled = ""
            gc_init = "G21 G90 G17 G54 \n"
            gc_end = "M02"
            compiled += gc_init
            compiled += self.pickMove(params)

            compiled += gc_end
            self._routines += compiled
        else:
            compiled = ""
            gc_init = "G21 G90 G17 G54 \n"
            gc_end = "M02"
            compiled += gc_init
            compiled += self.move(params)

            compiled += gc_end
            self._routines += compiled
        return compiled

    def createPickPath(self, col, plt, ColonyPicker = True): #redefine path from plate to wells or vice versa
        pick = ColonyPicker
        if pick:
            _ = list(zip(col,plt))
            #generate path for plate to wells
            #_ = [([colX, colY], [pltX, pltY])]
            return self.GcodeCompile(_)
            pass
        else:
            #generate path for wells to plates/media
            #params = [pltX, pltY, colX, colY
            _ = list(zip(plt,col))
            return self.GcodeCompile(_)
            pass

    def createPath(self, src, dest):
        _ = list(zip(src, dest))
        return self.GcodeCompile(_)

    def home(self):
        #get current position via serial
        path = self.move([[0, 0], [0, 0]])
        _machineState().write(path)

class _machineState(CoreXYInterface, metaclass=singleton): #access machine data

    def __init__(self):
        self.port = serial.Serial()

    def read(self):
        return self.port.readline()

    def write(self, data):
        if isinstance(data, (str, bytes, bytearray)):
            self.port.write(data)
        else:
            raise Exception("_machineState: Write data must be string, byte, or bytearray")

    def stream(self):
        self.write("\r\n\r\n")  # wake the device
        time.sleep(2)  # Wait to initialize
        self.port.flushInput()  # Flush startup text in serial input
        print('Sending gcode')

        # Stream g-code
        for line in CoreXYInterface._routines[0]:
            l = line.strip()  # Strip all EOL characters for streaming
            if (l.isspace() == False and len(l) > 0):
                print('Sending: ' + l)
                self.write(l + '\n')  # Send g-code block
                grbl_out = self.read()  # Wait for response with carriage return
                print(' : ' + grbl_out.strip())

    def getPosition(self):
        # returns devices current position
        # return position as [X, y]
        return []

if __name__ == "__main__":

    col = [[10,30], [20,30]] #[[X,Y], [X,Y]]
    plt = [[20,40], [30,50]]
    mech = CoreXYInterface()
    output = mech.createPickPath(col, plt, ColonyPicker=True)
    print(output)
