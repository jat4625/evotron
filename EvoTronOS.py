from NNRuntimes import SSD_FPN_pi_RUNTIME
from coreXYClass import CoreXYInterface, _machineState
from torr_utils import cropClass, cameraClass, evaluatorClass
from torr_utils.singletonType import singleton
"""
Front end for EvoTron, begins the instantiation of the Detector Object to run NN inference on images captured by the host device
"""
#begin detection
class EvoTron(object, metaclass=singleton):
    def __init__(self):
        self.cropCont = []
        self.cXY = CoreXYInterface()
        self.machine = _machineState()
        self.camera = cameraClass.Camera()
        self.detector = SSD_FPN_pi_RUNTIME.Pi_SSD_FPN(
            model_path='./Model/FPN(experimental)/frozen_inference_graph.pb',
            label_path='./labels/2label_map.pbtxt',
            NUM_CLASSES=2
        )
    def capturePlates(self):
        CoreXYInterface().LinearMove(axis=1, level=1)
        pass
    def _detectColonies(self):
        self.results = self.detector.SSD_FPN_inference(image="", threshold=0.3)
    def cropimages(self):
        for i in self.results:
            self.cropCont.append(cropClass.Crop(i))
    def eval(self):
        self.evl = evaluatorClass.Evaluator.add(img="")



