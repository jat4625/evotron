import cv2
import os
from object_detection.utils import label_map_util
class Pi_SSD_FPN(object):
    def __init__(self, model_path, label_path, NUM_CLASSES):
        self.model_path = model_path
        self.PATH_TO_LABELS = label_path
        self.NUM_CLASSES = NUM_CLASSES
        label_map = label_map_util.load_labelmap(self.PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(
            label_map,
            max_num_classes=NUM_CLASSES,
            use_display_name=True
        )
        self.category_index = label_map_util.create_category_index(categories)
        self.tf_model = cv2.dnn.readNetFromTensorflow(
            self.model_path,
            './Model/FPN(experimental)/FPN_graph.pbtxt'
        )
    def createPackage(self,x,y,w,h,img):
        return [img,[x,y,w,h]]

    def bbox(self, image, img_arr, detections):
        im_h, im_w, CLR_CH = img_arr.shape
        self.b_x = detections[3] * im_w
        self.b_y = detections[4] * im_h
        self.b_w = detections[5] * im_w
        self.b_h = detections[6] * im_h
        # Packed array contains img with bounding boxes and
        _packedArray = self.createPackage(self.b_x, self.b_y, self.b_w, self.b_h, cv2.rectangle(
            img_arr,
            (int(self.b_x), int(self.b_y)),
            (int(self.b_w), int(self.b_h)),
            (23, 230, 210),
            thickness=2
        ))
        #print(_packedArray)
        return _packedArray

    def SSD_FPN_inference(self, image, threshold=0.3):
        img = cv2.imread(image)
        # cv2.imshow('image', img)
        self.tf_model.setInput(cv2.dnn.blobFromImage(
            img,
            size=(512, 512),
            swapRB=True
        ))
        self.output = self.tf_model.forward()
        for detections in self.output[0, 0, :, :]:
            score = detections[2]
            if score > threshold:
                class_id = detections[1]
                # print(class_id,"class id")
                class_txt = self.category_index[int(class_id)]['name']
                print(str(str(class_id) + " " + str(detections[2]) + " " + class_txt))
                bbox_img = self.bbox(image, img, detections)
                cv2.imwrite('./FPNtest/FPN_'+ str(i) +'.jpg', bbox_img[0])
            else:
                bbox_img = img


        return bbox_img

if __name__ == "__main__":
    detector = Pi_SSD_FPN(
        model_path='./Model/FPN(experimental)/frozen_inference_graph.pb',
        label_path='./labels/2label_map.pbtxt',
        NUM_CLASSES=2
    )
    for i in range(1,14):
        image = './images/img.jpg'
        results = detector.SSD_FPN_inference(image, 0.1)
        print(results)


    cv2.destroyAllWindows()
