import numpy as np
import tensorflow as tf
import cv2
# get from models/research/Object_detection/utils
from object_detection.utils import label_map_util

PATH_TO_LABELS = "./labels/2label_map.pbtxt"
NUM_CLASSES = 2

class ObjectDetectorLite(object):
    def __init__(self, model_path="/Users/JATorr/Desktop/Machine_Learning/Images_to_label/General_colonies/gen_converted_FLT.tflite"):
        #build graph and load labels
        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
        use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)
        self.interpreter = tf.lite.Interpreter(model_path=model_path)
        self.interpreter.allocate_tensors()
        self.input_details = self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()


    def inference(self,image, threshold=0.02):
        img = cv2.resize(image,(300,300))
        img = np.expand_dims(img, axis=0)
        #normalize
        frame = (2.0/255.0) * img - 1.0
        image_tensor = frame.astype('float32')
        #invoke
        self.interpreter.set_tensor(self.input_details[0]['index'], image_tensor)
        self.interpreter.invoke()

        #get results
        bboxes = self.interpreter.get_tensor(
            self.output_details[0]['index']
        )

        classes = self.interpreter.get_tensor(
            self.output_details[1]['index']
        )

        scores = self.interpreter.get_tensor(
            self.output_details[2]['index']
        )

        num = self.interpreter.get_tensor(
            self.output_details[3]['index']
        )
        #derived from Tensorflow Object detection Utils np_box
        def _get_coordinates(image, boxes, classes, scores, threshold):
            Max_detects = 500
            if Max_detects == False:
                Max_detects = boxes.shape[0]
            N_boxes = min(Max_detects, boxes.shape[0])
            detect_boxes = []
            for i in range(N_boxes):
                if scores is None or scores[i] > threshold:
                    box = tuple(boxes[i].tolist())
                    Ymin, Xmin, Ymax, Xmax = box
                    IMG_H, IMG_W, _ = image.shape
                    L, R, top, bottom = [
                        int(z) for z in (
                            Xmin * IMG_W,
                            Xmax * IMG_W,
                            Ymin * IMG_H,
                            Ymax * IMG_H
                        )
                    ]
                    detect_boxes.append([
                        (L, top),
                        (R, bottom),
                        scores[i],
                        self.category_index[classes[i]]['name'] ]
                    )
            return detect_boxes

        return _get_coordinates(
            image,
            np.squeeze(bboxes[0]),
            np.squeeze(classes[0] + 1).astype(np.int32),
            np.squeeze(scores[0]),
            threshold=threshold
        )
    def close(self):
        tf.compat.v1.reset_default_graph()

        pass

if __name__ == "__main__":
    detector = ObjectDetectorLite()
    image = cv2.cvtColor(cv2.imread('\\Users\\JATorr\\Desktop\\Machine_Learning\\Images_to_label\\dataAugmentation\\3.jpg'), cv2.COLOR_BGR2RGB)
    result = detector.inference(image, 0.02)
    print(result)
    for obj in result:
        print('coordinates: {} {}. class: "{}". confidence: {:.2f}'.
              format(obj[0], obj[1], obj[3], obj[2]))

        cv2.rectangle(image, obj[0], obj[1], (0, 255, 0), 2)
        cv2.putText(image, '{}: {:.2f}'.format(obj[3], obj[2]),
                    (obj[0][0], obj[0][1] - 5),
                    cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 2)

    cv2.imwrite('r1.jpg', cv2.cvtColor(image, cv2.COLOR_RGB2BGR))

    detector.close()



