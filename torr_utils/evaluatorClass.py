import cv2
import numpy as np

"""Evaluator object for the manipulation and comparison of images"""
class Evaluator(object):
    imgObjs = [] #image data container

    def __init__(self, cropped_img):
        self.IMG = cropped_img

    """Instance Level method to perform an integration over an image"""
    def integrate(self):
        integrated_img = cv2.integral(self.IMG)
        return integrated_img
    """Class Level method to append created object to array of Evaluator comparable objects (created objects are of type Evaluator)"""
    @classmethod
    def add(cls, img):
        _ = cls(img)
        _.imgObjs.append(_)
        return _

    """Class Level method for debugging the static array in the Evaluator Class"""
    @classmethod
    def showArray(cls):
        print(cls.imgObjs)
    """{IN DEV} Class level method for comparing images according to a passed criteria"""
    @classmethod
    def comparator(cls):
        #define comparisons
        return {"brightest":x, "roundest": "{INDEV}", "largest": "{INDEV}"}
    @classmethod
    def process(cls):
        #call to perform evaluation ops
        pass

x = Evaluator.add([1,2,3,4,5])
y = np.array([
[1,2,3,4,5],
[1,2,3,4,5],
[1,2,3,4,5],
[1,2,3,4,5]
])
for i in range(10):
    _ = Evaluator.add(y)
for i in _.imgObjs:
    print(i.IMG)



