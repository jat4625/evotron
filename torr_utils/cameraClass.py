import numpy as np
import cv2
import glob
from torr_utils.singletonType import singleton
try:
    import picamera
except ImportError:
    pass

class Camera(object, metaclass=singleton):
    def __init__(self):
        self.workingDir = '/home/pi/Desktop/Captures/'
        self.saveDir = 'camera_data/'
        #get camera
        pass

    def calibrate(self):
        self.TermCriteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        _objPoints = np.zeros((7*7,3), np.float32)
        _objPoints[:,:2] = np.mgrid[0:7,0:7].T.reshape(-1,2)*2.5
        objArr =[]
        imgArr = []
        imgs = glob.glob('calibration_images/*.jpg')
        win_name = "Verify"
        cv2.namedWindow(win_name,cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty(win_name,cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        print("Retrieving Images")
        for f in imgs:
            img = cv2.imread(f)
            print(f)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            #Find corners
            ret, corners = cv2.findChessboardCorners(gray, (7,7),None)
            #when found add to Arrs
            if ret:
                objArr.append(_objPoints)
                corners2 = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), self.TermCriteria)
                imgArr.append(corners)
                #Display corners found
                cv2.drawChessboardCorners(img, (7,7), corners2, ret)
                cv2.imshow(win_name,img)
                cv2.waitKey(500)
            img1 = img
        cv2.destroyAllWindows()
        #===================Starting Calibration=====================================
        print("||Starting Calibration||")
        ret, cam_mat, dist, rvecs, tvecs = cv2.calibrateCamera(objArr, imgArr, gray.shape[::-1], None, None)
        print("Camera MATRIX")
        print(cam_mat)
        np.save(self.saveDir + 'cam_MAT.npy', cam_mat)
        print("DISTORTION COEFF")
        print(dist)
        np.save(self.saveDir + 'dist.npy', dist)
        print('R VECTORS')
        print(rvecs[2])
        print('T VECTORS')
        print(tvecs[2])
        print("||Ending Calibration||")

        h, w = img1.shape[:2]
        print("IMG Width & Height")
        print(w, h)
        newcam_MAT, ROI = cv2.getOptimalNewCameraMatrix(cam_mat, dist, (w, h),1, (w, h))
        print("REGION OF INTEREST")
        print(ROI)
        np.save(self.saveDir + 'roi.npy', ROI)
        print('NEW CAM MATRIX')
        np.save(self.saveDir + 'newcam_MAT.npy', newcam_MAT)
        print(np.load(self.saveDir + 'newcam_MAT.npy'))

        inv = np.linalg.inv(newcam_MAT)
        print("Inverse New Cam Matrix: ", inv)

        undist = cv2.undistort(img1, cam_mat, dist, None, newcam_MAT)
        cv2.imshow('img1', img1)
        cv2.waitKey(5000)
        cv2.destroyAllWindows()
        cv2.imshow('img1', undist)
        cv2.waitKey(5000)
        cv2.destroyAllWindows()





